package com.home.credit;

import javax.persistence.*;

@Entity
@Table(name = "MODULE")
public class Module {
	@Column
	private String moduleName;
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY) 
	private Long moduleOrder;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "USER_ID")
	private User user;
	
	public Module() {}
	public Module(String moduleName) {
		// TODO Auto-generated constructor stub
		this.moduleName = moduleName;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	
	public Long getModuleOrder() {
		return moduleOrder;
	}
	
	public void setModuleOrder(Long moduleOrder) {
		this.moduleOrder = moduleOrder;
	}
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
}
