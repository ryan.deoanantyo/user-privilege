package com.home.credit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
public class ModuleController {

	@Autowired
	private ModuleRepository repository;
	
	// Find
    @GetMapping("/modules")
    List<Module> findAll() {
        return repository.findAll();
    }

    // Find
    @GetMapping("/module/{userId}")
    public List<Module> getModuleByUserId(@PathVariable (value = "userId") Long userId) {
        return repository.findByUserId(userId);
    }
}
