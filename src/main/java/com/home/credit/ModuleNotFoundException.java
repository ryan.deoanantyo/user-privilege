package com.home.credit;

public class ModuleNotFoundException extends RuntimeException {

	public ModuleNotFoundException(Long id) {
		super("Module id not found : " + id);
	}

}
