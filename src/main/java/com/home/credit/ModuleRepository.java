package com.home.credit;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ModuleRepository extends JpaRepository<Module, Long> {
	List<Module> findByUserId(Long userId);
}
