package com.home.credit;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StartModuleApplication implements CommandLineRunner {
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private ModuleRepository moduleRepo;
	
	public static void main(String[] args) {
		SpringApplication.run(StartModuleApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {
        	User userA = new User("UserA");
        	User userB = new User("UserB");
        	
        	Module promo = new Module("PromoCard");
        	promo.setUser(userA);
        	Module category = new Module("CategoryCard");
        	category.setUser(userA);
        	Module flashSale = new Module("FlashSaleCard");
        	flashSale.setUser(userA);
        	Module history = new Module("HistoryCard");
        	history.setUser(userA);
        	Module news = new Module("NewsCard");
        	news.setUser(userA);
        	List<Module> listModuleA = new ArrayList<Module>();
        	listModuleA.add(promo);
        	listModuleA.add(category);
        	listModuleA.add(flashSale);
        	listModuleA.add(history);
        	listModuleA.add(news);
        	moduleRepo.saveAll(listModuleA);
        	
        	promo = new Module("PromoCard");
        	promo.setUser(userB);
        	news = new Module("NewsCard");
        	news.setUser(userB);
        	flashSale = new Module("FlashSaleCard");
        	flashSale.setUser(userB);
        	category = new Module("CategoryCard");
        	category.setUser(userB);
        	history = new Module("HistoryCard");
        	history.setUser(userB);
        	
        	List<Module> listModuleB = new ArrayList<Module>();
        	listModuleB.add(promo);
        	listModuleB.add(news);
        	listModuleB.add(flashSale);
        	listModuleB.add(category);
        	listModuleB.add(history);
        	moduleRepo.saveAll(listModuleB);
    }
}
