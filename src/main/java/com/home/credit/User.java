package com.home.credit;

import javax.persistence.*;

import java.util.List;

@Entity
@Table(name = "USER")
public class User {
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column private String userName;
	
	/*@OneToMany(mappedBy = "user")
	private List<Module> modules;*/
	
	public User() {}
	public User(String userName) {
		this.userName = userName;
	}
	
	public Long getUserID() {
		return id;
	}
	public void setUserID(Long userId) {
		id = userId;
	}
	
	/*public List<Module> getModules() {
		return modules;
	}
	public void setModules(List<Module> modules) {
		this.modules = modules;
	}*/
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
}
