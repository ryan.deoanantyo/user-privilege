package com.home.credit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
public class UserController {

	@Autowired
	private UserRepository repository;
	// Find
    @GetMapping("/users")
    List<User> findAll() {
    	System.out.println();
        return repository.findAll();
    }

    // Find
    @GetMapping("/user/{id}")
    User findOne(@PathVariable Long id) {
        return repository.findById(id)
                .orElseThrow(() -> new UserNotFoundException(id));
    }

}
